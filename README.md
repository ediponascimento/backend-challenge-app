# Backend-Challenge-App

## Instructions

1. Open a issue, post your references and/or a message, as you wish.
2. Fork this repo and push your commits to your forked repo.
3. When it's done, get in touch with your CosmoBots contact and send your repo link.

_You may use the issue conversation to ask questions._

## Specification

### User Story

As a user,  
I want to have a list of coworking locations, including it's latitude and longitude,  
So that I can retrieve the closest coworking to a particular location.

### Implementation Details

#### Database

| name                     | address              | Country | city           | state | zipcode  | lat        | lng         |
| ------------------------ | -------------------- | ------- | -------------- | ----- | -------- | ---------- | ----------- |
| Coworking São Paulo      | Does Not Matter, 123 | BR      | São Paulo      | SP    | 01000000 | -23.533773 | -46.625290  |
| Coworking Rio de Janeiro | Really, 345          | BR      | Rio de Janeiro | RJ    | 05400100 | -22.908333 | -43.196388  |
| Coworking New York       | Good Luck, 567       | USA     | New York City  | NY    | 10001000 | 40.730610  | -73.935242  |
| Coworking San Francisco  | Enjoy, 789           | USA     | San Francisco  | CA    | 94110000 | 37.733795  | -122.446747 |

> Reference: <https://www.latlong.net/>

#### Input

```json
{
  "myLocation": {
    "lat": 123,
    "lng": 123
  }
}
```

#### Output

```json
{
  "results": [
    {
      "name": "Coworking São Paulo",
      "address": "Does Not Matter, 123",
      "country": "BR",
      "city": "São Paulo",
      "state": "SP",
      "zipcode": "01000000",
      "lat": "-23.533773",
      "lng": "-46.625290"
    }
  ]
}
```

## Requirements

- Should be implemented as a AWS Lambda ✓
- Should make use of AWS DynamoDB ✓

## Obs

- You may change the database, input or output as you wish, providing the closest coworking is enough.

## Recomendations

- Setup a good development environment: <https://serverless.com/>
- Make use of good development practices: <https://12factor.net/>
- Tests are very important
- Use SOLID: <https://en.wikipedia.org/wiki/SOLID>
- Follow the REST Architecture: <https://restfulapi.net/>
- It's good to write nice git commit messages: <http://karma-runner.github.io/3.0/dev/git-commit-msg.html>
